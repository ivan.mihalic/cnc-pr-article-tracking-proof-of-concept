# CNC PR article tracking (proof of concept)

### !THIS IS NOT FOR PRODUCTION IMPLEMENTATION. IT IS ONLY FOR DEMO PURPOSES!


This app is proof of concept for counting article's impressions and clicks on URL links (to customer web) inside the article.

Impression counting is made by inserting image (into PR article) with exact URL of image `src`. The URL has unique query parameter `hash`. 
```
FOR EXAMPLE:
<img src="http://domain.tld/c/i?hash={hash} />
```

Link click counting is made by linking to our URL. The URL has unique query parameter `hash` and `url` which is final destination for user. 
```
FOR EXAMPLE:
<a href="http://domain.tld/c/c?hash={hash}&url={destination_url}">LINK</a>
```

API catches these exact URLs and stores view/click count to DB based on `hash` to article

_In example_ `http://domain.tld` _is replaced with_ `http://localhost:[port]`

#### Example requirements
Tested on MacOS.

I think that DEV script definition in package.json will not be compatible with Windows (some modification required).{: .note}

```
- PostgreSQL as DB engine (tested on version 9.5)`
- NodeJS for serving webpage and for API (tested on 12.18.x version)
```

##### For working example
1. init DB by executing `[project_root]/db-init.sql`
2. install packages by executing `yarn install` or `npm install` command (if npm remove `yarn.lock` first)
3. set up `.env.development` config file for DB connection and website port (default: 3000)
4. execute `yarn dev` or `npm dev` command for run the website

The example website runs on `http://localhost:[port]` default: `http://localhost:3000`
