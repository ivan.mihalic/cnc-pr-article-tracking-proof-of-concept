-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2
-- PostgreSQL version: 9.5
-- Project Site: pgmodeler.io
-- Model Author: ---

-- object: test_user | type: ROLE --
-- DROP ROLE IF EXISTS test_user;
CREATE ROLE test_user WITH 
	LOGIN
	ENCRYPTED PASSWORD 'password123';
-- ddl-end --


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: test_db | type: DATABASE --
-- -- DROP DATABASE IF EXISTS test_db;
-- CREATE DATABASE test_db
-- 	OWNER = postgres;
-- -- ddl-end --
-- 

-- object: public.counter_item | type: TABLE --
-- DROP TABLE IF EXISTS public.counter_item CASCADE;
CREATE TABLE public.counter_item (
	id serial NOT NULL,
	hash text NOT NULL,
	created timestamptz DEFAULT NULL,
	deleted timestamptz DEFAULT NULL,
	name text DEFAULT NULL,
	google_line_item_id text DEFAULT NULL,
	start_counting date DEFAULT NULL,
	end_counting date DEFAULT NULL,
	target_url text DEFAULT NULL,
	CONSTRAINT counter_item_pk PRIMARY KEY (id),
	CONSTRAINT counter_item_hash_uq UNIQUE (hash)

);
-- ddl-end --
-- ALTER TABLE public.counter_item OWNER TO postgres;
-- ddl-end --

-- object: public.counter_count | type: TABLE --
-- DROP TABLE IF EXISTS public.counter_count CASCADE;
CREATE TABLE public.counter_count (
	id serial NOT NULL,
	id_counter_item integer,
	date date DEFAULT NULL,
	impression_count integer NOT NULL DEFAULT 0,
	click_count integer NOT NULL DEFAULT 0,
	CONSTRAINT counter_count_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.counter_count OWNER TO postgres;
-- ddl-end --

-- object: counter_item_fk | type: CONSTRAINT --
-- ALTER TABLE public.counter_count DROP CONSTRAINT IF EXISTS counter_item_fk CASCADE;
ALTER TABLE public.counter_count ADD CONSTRAINT counter_item_fk FOREIGN KEY (id_counter_item)
REFERENCES public.counter_item (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


