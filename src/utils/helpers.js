const lodash = require('lodash');
const moment = require('moment-timezone');
const isEqual = lodash.isEqual;

moment.tz.setDefault("Europe/Prague");
moment.locale('cs');

const isEmpty = (value) => {

	if(value === undefined || value === null || value === ''){

		return true;

	} else if(Array.isArray(value) && value.length === 0){

		return true;

	}else if(typeof value == 'object' && Object.keys(value).length === 0){

		return true;

	}

	return false;
};
const isArray = (value) => {

	if(value === undefined || value === null || value === ''){

		return false;

	} else if(Array.isArray(value)){

		return true;

	}

	return false;

};
const isObject = (value) => {

	if(value === undefined || value === null || value === ''){

		return false;

	}else if(typeof value == 'object' && !Array.isArray(value)){

		return true;

	}

	return false;
};
const get = (obj, path, defaultValue) => {

	if(typeof path !== 'string'){
		return defaultValue;
	}

	const result = String.prototype.split.call(path, /[,[\].]+?/)
	.filter(Boolean)
	.reduce((res, key) => (res !== null && res !== undefined) ? res[key] : res, obj);

	return (result === undefined || result === obj || isEmpty(result)) ? defaultValue : result;

};

function hash(length){
	var result           = '';
	var characters       = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz23456789';
	var charactersLength = characters.length;
	for ( var i = 0; i < length; i++ ) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}
module.exports = {
	hash,
	omit:  lodash.omit,
	isEmpty,
	get,
	isEqual,
	repeat: lodash.repeat,
	isArray,
	isObject,
	moment
};
