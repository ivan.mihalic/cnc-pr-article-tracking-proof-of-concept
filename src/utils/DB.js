const _ = require('./helpers');
const {Pool, types} = require('pg');

const named = require('node-postgres-named');
//pretypovani databazoveho BigInt - defaultne knihovna "pg" vraci BigInt jako string
types.setTypeParser(20, function(val) {
	return parseInt(val, 10);
});
//pretypovani databazoveho Decimal - defaultne knihovna "pg" vraci Decimal jako string
types.setTypeParser(1700, function(val) {
	return parseFloat(val);
});
//pretypovani databazoveho DATE - defaultne knihovna "pg" vraci Decimal jako string
types.setTypeParser(1082, function(val) {
	return val;
});
const pool = new Pool();

function checkResult(response){

	return new Promise((resolve, reject) => {

		if (
			_.isArray(response) && response.length === 1
			&& !_.isEmpty(response[0].code)
			&& !isNaN(parseInt(response[0].code, 10))
			&& parseInt(response[0].code, 10) < 0
		) {

			if(_.isObject(response[0].error)){
				return reject(response[0].error);
			}
			if(typeof response[0].error === 'string') {

				try {
					return reject(JSON.parse(response[0].error));
				} catch (e) {
					return reject({});
				}
			}

			return reject({});
		}

		resolve(response);

	});

}

let object = {
	Query: (sql, params) => {
		return new Promise((resolve, reject) => {

			let client;

			pool.connect()
			.then((_client) => {
				client = _client;
				named.patch(client);

				return client.query(`SET search_path TO ${process.env.DB_SCHEMA}`);

			})
			.then(() => {

				return client.query(sql, params);

			})
			.then(result => {
				let rows = _.get(result, 'rows', []);
				return checkResult(rows);
			})
			.then(result => {
				if (client) {
					client.release();
				}
				resolve(result);

			})
			.catch(error => {

				if (client) {
					client.release();
				}

				reject(error);

			});


		});

	},
	Transaction: {
		/**
		 *
		 * @returns {Promise<object>}
		 */
		begin: () => {

			return new Promise((resolve, reject) => {

				let client;

				pool.connect()
				.then(_client => {

					client = _client;
					named.patch(client);

					return client.query('BEGIN');

				})
				.then(() => {

					resolve(client);

				})
				.catch(error => {

					if(client) {
						client.release();
					}
					reject(error);

				});


			});

		},
		/**
		 *
		 * @param {object} client
		 * @returns {Promise<boolean|object>}
		 */
		commit: (client) => {

			return new Promise((resolve, reject) => {

				client.query('COMMIT')
				.then(() => {

					if(client) {
						client.release();
					}
					resolve(true);

				})
				.catch(error => {

					if(client) {
						client.release();
					}
					reject(error);

				});


			});

		},
		/**
		 *
		 * @param {object} client
		 * @returns {Promise<boolean|object>}
		 */
		rollback: (client) => {

			return new Promise((resolve, reject) => {

				client.query('ROLLBACK')
				.then(() => {

					if(client) {
						client.release();
					}
					resolve(true);

				})
				.catch(error => {

					if(client) {
						client.release();
					}
					reject(error);

				});


			});

		},
		/**
		 *
		 * @param {object} client
		 * @param {string} sql
		 * @param {object} params
		 * @returns {Promise}
		 */
		query: (client, sql, params) => {

			return new Promise((resolve, reject) => {

				client.query(`SET search_path TO ${process.env.DB_SCHEMA}`)
				.then((r) => {

					return client.query(sql, params);

				})
				.then((result) => {

					let rows = _.get(result, 'rows', []);

					return checkResult(rows);

				})
				.then(result => {

					resolve(result);

				})
				.catch(error => {

					reject(error);

				});


			});

		}
	}
};


object.query = object.Query;
object.QueryOne = object.queryOne = (sql, params) => {

	return new Promise((resolve, reject) => {

		object.Query(sql, params)
			.then(result => {
				if(result.length !== 1){
					return resolve(null);
				}
				return resolve(result[0]);
			})
			.catch(error => {

				reject(error);
			});


		});

};
object.QueryOneOrError = object.queryOneOrError = (sql, params, error) => {

	return new Promise((resolve, reject) => {

		object.Query(sql, params)
			.then(result => {

				if(result.length !== 1){
					return reject(error);
				}
				resolve(result[0]);
			})
			.catch(_error => {

				reject(_error);
			});


		});

};
object.Insert = (sql, params, error) => {

	return new Promise((resolve, reject) => {

		object.Query(sql, params)
			.then(result => {

				if(result.length !== 1){
					return reject({message: 'No result'});
				}
				let new_object = result[0];
				if(_.isEmpty(new_object, 'id')){
					return reject({message: 'No result id'});
				}

				resolve(_.get(new_object, 'data', new_object));

			})
			.catch(_error => {

				reject(_error);
			});


		});

};

object.Transaction.queryOne = (client,sql, params) => {

	return new Promise((resolve, reject) => {

		object.Transaction.query(client, sql, params)
		.then(result => {
			if(result.length !== 1){
				return resolve(null);
			}
			resolve(result[0]);
		})
		.catch(error => {
			reject(error);
		});


	});

};

object.Transaction.insert = (client, sql, params) => {

	return new Promise((resolve, reject) => {

		object.Transaction.query(client, sql, params)
		.then(result => {

			if(result.length !== 1){
				return reject({message: 'No result'});
			}
			let new_object = result[0];
			if(_.isEmpty(new_object, 'id')){
				return reject({message: 'No result id'});
			}

			resolve(_.get(new_object, 'data', new_object));

		})
		.catch(error => {
			reject(error);
		});


	});

};

object.Transaction.update = (client, sql, params) => {

	return new Promise((resolve, reject) => {

		object.Transaction.query(client, sql, params)
		.then(result => {
			// TODO: prozatim nereit a vse dat jako resolved
			return resolve();

			if(result.length !== 1){
				return reject({message: 'No result'});
			}
			let updated_object = result[0];

			if(_.isEmpty(updated_object, 'id')){
				return reject({message: 'No result id'});
			}

			resolve(_.get(updated_object, 'data', updated_object));
		})
		.catch(error => {
			reject(error);
		});


	});

};
object.Transaction.delete = (client, sql, params) => {

	return new Promise((resolve, reject) => {

		object.Transaction.query(client, sql, params)
		.then(result => {
			if(result.length !== 1){
				return reject({message: 'No result'});
			}
			resolve(result[0]);
		})
		.catch(error => {
			reject(error);
		});


	});

};
object.Transaction.queryOneOrError = (client, sql, params, error) => {

	return new Promise((resolve, reject) => {

		object.Transaction.query(client, sql, params)
			.then(result => {
				if(result.length !== 1){
					reject(error)
				}
				resolve(result[0]);
			})
			.catch(_error => {
				reject(_error);
			});


		});

};

module.exports = object;
