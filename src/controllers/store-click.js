/**
 * @format
 */
/**
 * @format
 */

const DB = require('../utils/DB');
const _ = require('../utils/helpers');

function existsCounter(hash) {
    return new Promise((resolve, reject) => {
        DB.Query(
            `SELECT * FROM "counter_item" 
                WHERE hash = $hash AND deleted IS NULL`,
            {hash},
        )
            .then(exists => {
                if (exists.length === 1) {
                    if (
                        exists[0].start_counting &&
                        _.moment().isBefore(_.moment(exists[0].start_counting, 'YYYY-MM-DD'), 'day')
                    ) {
                        return reject('current date is before start');
                    }
                    if (
                        exists[0].end_counting &&
                        _.moment().isAfter(_.moment(exists[0].end_counting, 'YYYY-MM-DD'), 'day')
                    ) {
                        return reject('current date is after end');
                    }
                    resolve(exists[0]);
                } else {
                    reject('no counter found');
                }
            })
            .catch(error => {
                console.log('ERROR existsCounter', error);
                reject(error);
            });
    });
}

function existsOne(hash, date) {
    return new Promise((resolve, reject) => {
        DB.Query(
            `SELECT * 
					  FROM "counter_count" 
							WHERE id_counter_item = (SELECT id FROM "counter_item" WHERE hash = $hash)
							AND date = '${date}'`,
            {hash},
        )
            .then(exists => {
                console.log({exists});
                if (exists.length === 1) {
                    resolve(exists[0]);
                } else {
                    resolve(null);
                }
            })
            .catch(error => {
                console.log('ERROR existsOne', error);
                reject(error);
            });
    });
}

function storeClick(hash) {
    // let date = _.moment().format('YYYY-MM-DD');
    let date = _.moment().subtract(10, 'day').format('YYYY-MM-DD');

    return new Promise((resolve, reject) => {
        const updateSQL = `
			UPDATE "counter_count" SET click_count = click_count +1
				WHERE id = $id
					RETURNing *
		`;
        const insertSQL = `
			INSERT INTO "counter_count" (id_counter_item, date, click_count)
			VALUES ($id_counter_item, $date, 1)
								RETURNing *
		`;

        let counter;
        existsCounter(hash, date)
            .then(_counter => {
                counter = _counter;
                return existsOne(hash, date);
            })
            .then(exists => {
                if (exists) {
                    console.log('UPDATING');
                    return DB.Query(updateSQL, {id: exists.id});
                } else {
                    console.log('CREATING', counter);
                    return DB.Query(insertSQL, {id_counter_item: counter.id, date});
                }
            })
            .then(result => {
                resolve(result);
            })
            .catch(error => {
                console.log('ERROR storeImpression', error);
                reject(error);
            });
    });
}

module.exports = storeClick;
