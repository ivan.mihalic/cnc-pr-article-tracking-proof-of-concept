/**
 * @format
 */

const DB = require('../utils/DB');

function listCounter(filter) {
    let SQL = `
			SELECT * FROM "counter_item" WHERE deleted IS NULL ORDER BY created DESC
		`;
    let SQLcounts = `
			SELECT SUM(impression_count) AS impression_count, 
			  SUM(click_count) AS click_count, id_counter_item
			FROM "counter_count" 
                GROUP BY id_counter_item
		`;
    let sqlParams = {};
    let sqlCountsParams = {};

    if (filter) {
        let sqlCond = '';
        let sqlCountsCond = '';
        if (filter.id) {
            sqlCond = ` AND id = $id`;
            sqlParams.id = filter.id;
            sqlCountsCond = ' AND id_counter_item = $id_counter_item';
            sqlCountsParams.id_counter_item = filter.id;
        }
        if (filter.date_from) {
            // sqlCond = ` AND start_counting >= '${filter.date_from}'::date`;
            sqlCond = ` AND start_counting >= $date_from`;
            sqlParams.id = filter.date_from;
        }
        if (filter.date_to) {
            // sqlCond = ` AND start_counting >= '${filter.date_from}'::date`;
            sqlCond = ` AND start_counting >= $date_to`;
            sqlParams.id = filter.date_to;
        }
        SQL = `
			SELECT * FROM "counter_item" WHERE deleted IS NULL ${sqlCond} ORDER BY created DESC
		`;
        SQLcounts = `
			SELECT SUM(impression_count) AS impression_count, 
			  SUM(click_count) AS click_count, date
			FROM "counter_count" 
			WHERE true ${sqlCountsCond}
                GROUP BY date
                  ORDER BY date
		`;
    }

    return new Promise((resolve, reject) => {
        let records = [];
        DB.Query(SQL, sqlParams)
            .then(result => {
                records = result;
                if (filter && records.length !== 1) {
                    throw new Error('Invalid id');
                }

                return DB.Query(SQLcounts, sqlCountsParams);
            })
            .then(counts => {
                let items = records.map(record => {
                    let impression_count = 0;
                    let click_count = 0;
                    let foundCount = counts.find(item => item.id_counter_item === record.id);
                    if (foundCount) {
                        impression_count = foundCount.impression_count || impression_count;
                        click_count = foundCount.click_count || click_count;
                    }
                    return {
                        ...record,
                        google_line_item_id: record.google_line_item_id || '-',
                        target_url: record.target_url ? 'YES' : '-',
                        impression_count,
                        click_count,
                    };
                });

                if (filter) {
                    items = counts.map(item => {
                        return `${item.date},${records[0].hash},${item.impression_count},${item.click_count}`;
                    });
                    items = [`date,hash,impression_count,click_count`, ...items];
                    resolve({
                        name: records[0].name,
                        hash: records[0].hash,
                        items: items.join('\n'),
                    });
                }

                resolve(items);
            })
            .catch(error => {
                console.log('ERROR listCounter', error);
                reject(error);
            });
    });
}

module.exports = listCounter;
