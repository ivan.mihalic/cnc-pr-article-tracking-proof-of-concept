/**
 * @format
 */

const DB = require('../utils/DB');
const _ = require('../utils/helpers');

function createCounter(data) {
    return new Promise((resolve, reject) => {
        const SQL = `
			INSERT INTO "counter_item" (hash, created,name,google_line_item_id,start_counting, end_counting,target_url) 
			VALUES ($hash, $created,$name,$google_line_item_id,$start_counting, $end_counting, $target_url)
			RETURNING *
		`;

        let hash = _.hash(10);

        let inputData = {
            ...data,
            created: new Date().toISOString(),
            hash,
        };
        if (!inputData.start_counting) {
            inputData.start_counting = null;
        } else {
            inputData.start_counting = inputData.start_counting.substring(0, 10);
        }
        if (!inputData.end_counting) {
            inputData.end_counting = null;
        } else {
            inputData.end_counting = inputData.end_counting.substring(0, 10);
        }

        DB.Query(SQL, inputData)
            .then(result => {
                resolve(result[0]);
            })
            .catch(error => {
                console.log('ERROR listCounter', error);
                reject(error);
            });
    });
}

module.exports = createCounter;
