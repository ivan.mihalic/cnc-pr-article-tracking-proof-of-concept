/**
 * @format
 */
var express = require('express');
var _ = require('../utils/helpers');
var listCounter = require('../controllers/list-counter');
var router = express.Router();
// console.log(encodeURIComponent('http://seznam.cz'));

router.route('/').get((req, res) => {
    listCounter()
        .then(counterList => {
            res.render('list-counter', {
                title: 'Impressions & clicks',
                counterList: counterList.map(item => {
                    return {
                        ...item,
                        created: _.moment(item.created).format('YYYY-MM-DD - HH:mm'),
                        start_counting: item.start_counting ? _.moment(item.start_counting).format('YYYY-MM-DD') : '-',
                        end_counting: item.end_counting ? _.moment(item.end_counting).format('YYYY-MM-DD') : '-',
                    };
                }),
                counterListCount: counterList.length,
            });
        })
        .catch(error => {
            console.log('ERROR route /', error);
            res.render('error');
        });
});
router.route('/csv').get((req, res) => {
    let filter = {
        id: req.query.id,
        date_from: req.query.date_from || null,
        date_to: req.query.date_to || null,
    };
    if (!req.query.id) {
        return res.render('error', {message: 'No ID of counter is present'});
    }

    listCounter(filter)
        .then(data => {
            let filename = data.name.toLowerCase();
            filename = filename.split(' ').join('-');
            filename += '_' + data.hash;

            res.set('Content-Type', 'text/csv');
            res.setHeader('Content-disposition', 'attachment; filename=' + filename + '.csv');
            res.send(Buffer.from(data.items));
        })
        .catch(error => {
            console.log('ERROR route /', error);
            res.render('error');
        });
});
module.exports = router;
