/**
 * @format
 */
var express = require('express');
var path = require('path');
var _ = require('../utils/helpers');
var storeImpression = require('../controllers/store-impression');
var storeClick = require('../controllers/store-click');
var router = express.Router();

router.route('/i').get((req, res) => {
    let trackImageUrl = path.resolve(__dirname, '..', 'tr.gif');
    const hash = req.query.hash || null;

    if (hash) {
        storeImpression(hash)
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log(error);
            });
    }
    res.set('Content-Type', 'image/gif');
    res.sendFile(trackImageUrl);
});
//
router.route('/c').get((req, res) => {
    const targetURL = req.query.url || null;
    const hash = req.query.hash || null;

    if (targetURL && hash) {
        storeClick(hash)
            .then(a => {
                console.log(a);
            })
            .catch(e => {
                console.log(e);
            });
    }
    //presmerovat na cilovou url
    if (targetURL) {
        res.redirect(targetURL);
    } else {
        res.sendStatus(400);
    }
});

module.exports = router;
