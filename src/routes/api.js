/**
 * @format
 */
var express = require('express');
var createCounter = require('../controllers/create-counter');
var _ = require('../utils/helpers');
var router = express.Router();

router.route('/create-counter').post((req, res) => {
    createCounter(req.body)
        .then(result => {
            result.created = _.moment(result.created).format('YYYY-MM-DD - HH:mm');
            result.start_counting = result.start_counting ? _.moment(result.start_counting).format('YYYY-MM-DD') : null;
            result.end_counting = result.end_counting ? _.moment(result.end_counting).format('YYYY-MM-DD') : null;
            res.status(200).json(result);
        })
        .catch(error => {
            console.log('ERROR route /api/create-counter', error);
            res.status(400).json(error);
        });
});

module.exports = router;
