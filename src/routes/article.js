/**
 * @format
 */
var express = require('express');
var DB = require('../utils/DB');
var router = express.Router();
// console.log(encodeURIComponent('http://seznam.cz'));

router.route('/:hash').get((req, res) => {
    DB.Query(`SELECT * FROM "counter_item" WHERE hash = $1`, [req.params.hash])
        .then(result => {
            if (result.length === 1) {
                res.render('article', {
                    hash: req.params.hash,
                    name: result[0].name,
                    url: result[0].target_url
                        ? `http://localhost:3000/c/c?hash=${result[0].hash}&url=${result[0].target_url}`
                        : null,
                });
            } else {
                res.render('article', {
                    hash: null,
                    name: 'Invalid article',
                });
            }
        })
        .catch(error => {
            res.render('error');
        });
});
module.exports = router;
